# Operator Advisor

Library to model and advice a forest operator for a [digital twin application prototype](https://gitlab.com/forestry-operator-digital-twin/simulator). The library transforms raw data in JSON format following the pattern presented into this [repository](https://gitlab.com/forestry-operator-digital-twin/data/-/blob/main/readme.md) into an observation trait. The observation is semantically multidimensional it  and is divided into the following dimension:
  - Tree observations (can be a distance or the number of trees close to the machine)
  - Excavator speed magnitude
  - Excavator speed direction
  - Excavator angular speed
  - Stick extension distance
  - Stick angle
  - Prehension hand state (open, close)
