#[cfg(test)]
mod tests {
    use crate::Data;
    use std::path::PathBuf;

    #[test]
    fn test_new_data() {
        let data = Data::from_file(PathBuf::from("src/test_data.json"));
        data.unwrap();
    }

}
