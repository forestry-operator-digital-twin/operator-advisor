use serde::{Deserialize, Serialize};
use serde_json;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::vec::Vec;
pub mod model;
pub mod observation;
mod tests;

pub type Position = [f32; 3];

/// data from the simulator https://gitlab.com/forestry-operator-digital-twin/data
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Data {
    pub data: Vec<InternalData>,
    pub metadata: Metadata,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct InternalData {
    pub step: i32,
    pub tree_1: Position,
    pub tree_2: Position,
    pub tree_3: Position,
    pub tree_4: Position,
    pub tree_5: Position,
    pub tree_6: Position,
    pub tree_7: Position,
    pub tree_8: Position,
    pub tree_9: Position,
    pub tree_10: Position,
    pub position_excavator: Position,
    pub position_stick: Position,
    pub position_boom: Position,
    pub position_hand: Position,
    pub position_finger_1: Position,
    pub position_finger_2: Position,
    pub angular_speed_chassis: f32,
    pub speed_excavator: Position,
    pub angular_pos_chassis: f32,
}

/// metadata of the simulation
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Metadata {
    pub time_interval: f32,
    pub unit_of_time: String,
    pub unit_of_distance: String,
    pub unit_of_angle: String,
    pub starting_time_of_recording_unix: f32,
}

impl Data {
    pub fn from_file(path: PathBuf) -> Result<Self, String> {
        match File::open(path) {
            Err(e) => Err(format!("{}", e)),
            Ok(mut file) => {
                let mut json_string = String::new();
                if let Err(e) = file.read_to_string(&mut json_string) {
                    return Err(format!("{}", e));
                }
                let json: Result<Self, serde_json::error::Error> =
                    serde_json::from_str(json_string.as_str());
                match json {
                    Err(e) => Err(format!("{}", e)),
                    Ok(res) => Ok(res),
                }
            }
        }
    }

    pub fn from_string(json_string: &String) -> Result<Self, String> {
        let json: Result<Self, serde_json::error::Error> =
            serde_json::from_str(json_string.as_str());
        match json {
            Err(e) => Err(format!("{}", e)),
            Ok(res) => Ok(res),
        }
    }
}
