use super::{Observation, TreeObservation};

use super::util::{angle, distance_between_points, distance_between_points_2d, norm};
use crate::InternalData as Data;

#[derive(Debug, Clone)]
/// create an observation with continuous values that implement `Observation` trait
pub struct ContinuousObservationNoTree {
    finger_open: bool,
    excavator_speed: f32,
    excavator_direction: f32,
    excavator_angular_speed: f32,
    stick_position: f32,
    stick_angle: f32,
}

impl ContinuousObservationNoTree {
    pub fn new(data: &[Data; 2]) -> Self {
        Self {
            finger_open: Self::observation_finger(&data[1]),
            excavator_speed: Self::observation_exacavator_speed(data),
            excavator_direction: Self::observation_excavator_direction(data),
            excavator_angular_speed: Self::observation_exacavator_angular_speed(data),
            stick_position: Self::observation_stick_position(data),
            stick_angle: Self::observation_stick_angle(data),
        }
    }

    fn observation_finger(data: &Data) -> bool {
        let distance = distance_between_points(data.position_finger_1, data.position_finger_2);

        distance <= DISTANCE_CLOSE_FINGER
    }

    fn observation_exacavator_speed(data: &[Data; 2]) -> f32 {
        (norm(data[0].speed_excavator) + norm(data[1].speed_excavator)) / 2f32
    }

    fn observation_excavator_direction(data: &[Data; 2]) -> f32 {
        (angle(data[0].speed_excavator) + angle(data[1].speed_excavator)) / 2f32
    }

    fn observation_exacavator_angular_speed(data: &[Data; 2]) -> f32 {
        (data[0].angular_speed_chassis + data[1].angular_speed_chassis) / 2f32
    }

    fn observation_stick_position(data: &[Data; 2]) -> f32 {
        (distance_between_points_2d(data[0].position_excavator, data[0].position_finger_1)
            + distance_between_points_2d(data[0].position_excavator, data[1].position_finger_1))
            / 2f32
    }

    fn observation_stick_angle(data: &[Data; 2]) -> f32 {
        let angle = (data[0].angular_pos_chassis + data[1].angular_pos_chassis) / 2f32;
        // convert rad to deg
        angle * (180f32 / std::f32::consts::PI)
    }
}

impl Observation for ContinuousObservationNoTree {
    fn to_vec(&self) -> Option<Vec<f32>> {
        let obs: Vec<f32> = vec![
            self.excavator_speed,
            self.excavator_direction,
            self.excavator_angular_speed,
            self.stick_position,
            self.stick_angle,
            self.finger_open as i32 as f32,
        ];
        Some(obs.clone())
    }
    /// number of trees at proximity of the operator
    fn tree_observation(&self) -> TreeObservation {
        TreeObservation::None
    }
    fn excavator_speed_observation(&self) -> String {
        format!("{:.2}", self.excavator_speed)
    }
    fn excavator_angular_speed_observation(&self) -> String {
        format!("{:.2}", self.excavator_angular_speed)
    }
    fn stick_extension_observation(&self) -> Option<String> {
        Some(format!("{:.2}", self.stick_position))
    }
    fn stick_angle_observation(&self) -> String {
        format!("{:.2}", self.stick_angle)
    }
    fn finger_state_observation(&self) -> Option<String> {
        Some(format!("{}", self.finger_open))
    }
    fn excavator_speed_direction_observation(&self) -> Option<String> {
        None
    }
}

const DISTANCE_CLOSE_FINGER: f32 = 0.4;
