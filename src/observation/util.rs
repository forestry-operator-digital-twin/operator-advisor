use crate::observation::Observation;
use crate::InternalData as Data;
use crate::Position;
use std::fmt;
use strum_macros::{EnumCount as EnumCountMacro, EnumIter};

pub fn distance_between_points(p1: Position, p2: Position) -> f32 {
    let x = p2[0] - p1[0];
    let y = p2[1] - p1[1];
    let z = p2[2] - p1[2];
    f32::sqrt(f32::powi(x, 2) + f32::powi(y, 2) + f32::powi(z, 2))
}

pub fn distance_between_points_2d(p1: Position, p2: Position) -> f32 {
    let x = p2[0] - p1[0];
    let y = p2[1] - p1[1];
    f32::sqrt(f32::powi(x, 2) + f32::powi(y, 2))
}

pub fn norm(p: Position) -> f32 {
    f32::sqrt(f32::powi(p[0], 2) + f32::powi(p[1], 2) + f32::powi(p[2], 2))
}

pub fn angle(p: Position) -> f32 {
    (p[1] / p[0]).atan() * 180f32 / std::f32::consts::PI
}

pub fn data_trees_to_vector(data: &Data) -> Vec<Position> {
    vec![
        data.tree_1,
        data.tree_2,
        data.tree_3,
        data.tree_4,
        data.tree_5,
        data.tree_6,
        data.tree_7,
        data.tree_8,
        data.tree_9,
        data.tree_10,
    ]
}

pub fn convert_observations_to_int(obs: &Vec<Box<dyn Observation>>) -> Vec<usize> {
    obs.iter()
        .map(|x| x.to_int().expect("this observation is not discrete"))
        .collect()
}

pub fn convert_observations_to_feature_vector(obs: &Vec<Box<dyn Observation>>) -> Vec<Vec<f32>> {
    obs.iter()
        .map(|x| x.to_vec().expect("this observation is not continous"))
        .collect()
}

pub fn get_feature_from_observation(obs: &Vec<Vec<f32>>, feature: &Feature) -> Vec<f32> {
    let i = match feature {
        Feature::ContinuousFeature(i) => i.clone() as usize,
        Feature::ContinuousFeatureClosestTree(i) => i.clone() as usize,
        Feature::ContinuousFeatureNoTree(i) => i.clone() as usize,
        Feature::ContinuousFeatureSimpleMachineInformation(i) => i.clone() as usize,
    };
    obs.iter().map(|x| x[i]).collect()
}

#[derive(EnumCountMacro, Clone, EnumIter, Debug)]
pub enum ContinuousFeature {
    Tree1 = 0,
    Tree2,
    Tree3,
    Tree4,
    Tree5,
    Tree6,
    Tree7,
    Tree8,
    Tree9,
    Tree10,
    ExcavatorSpeed,
    ExcavatorDirection,
    ExcavatorAngularSpeed,
    StickExtension,
    CabinAngle,
    FingerState,
}

#[derive(EnumCountMacro, Clone, EnumIter, Debug)]
pub enum ContinuousFeatureClosestTree {
    ExcavatorSpeed = 0,
    ExcavatorDirection,
    ExcavatorAngularSpeed,
    StickExtension,
    CabinAngle,
    FingerState,
    ClosestTree,
}

#[derive(EnumCountMacro, Clone, EnumIter, Debug)]
pub enum ContinuousFeatureNoTree {
    ExcavatorSpeed = 0,
    ExcavatorDirection,
    ExcavatorAngularSpeed,
    StickExtension,
    CabinAngle,
    FingerState,
}

#[derive(EnumCountMacro, Clone, EnumIter, Debug)]
pub enum ContinuousFeatureSimpleMachineInformation {
    ExcavatorSpeed = 0,
    ExcavatorDirection,
    ExcavatorAngularSpeed,
    CabinAngle,
}

#[derive(EnumCountMacro, Clone, Debug)]
pub enum Feature {
    ContinuousFeature(ContinuousFeature),
    ContinuousFeatureClosestTree(ContinuousFeatureClosestTree),
    ContinuousFeatureNoTree(ContinuousFeatureNoTree),
    ContinuousFeatureSimpleMachineInformation(ContinuousFeatureSimpleMachineInformation),
}

impl fmt::Display for Feature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            Feature::ContinuousFeature(i) => format!("{:?}", i),
            Feature::ContinuousFeatureClosestTree(i) => format!("{:?}", i),
            Feature::ContinuousFeatureNoTree(i) => format!("{:?}", i),
            Feature::ContinuousFeatureSimpleMachineInformation(i) => format!("{:?}", i),
        };
        write!(f, "{}", val)
    }
}
