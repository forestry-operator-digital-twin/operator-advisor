pub mod discrete_observation;
pub mod md_closest_tree_continuous_observation;
pub mod md_continuous_observation;
pub mod md_continuous_observation_no_tree_information;
pub mod md_continuous_observation_only_simple_machine_information;

pub mod util;

use crate::InternalData as Data;

use core::fmt::Debug;
use discrete_observation::DiscreteObservation;
use md_closest_tree_continuous_observation::ContinuousObservationClosestTree;
use md_continuous_observation::ContinuousObservation;
use md_continuous_observation_no_tree_information::ContinuousObservationNoTree;
use md_continuous_observation_only_simple_machine_information::ContinuousObservationSimpleMachineInformation;

use serde::{Deserialize, Serialize};

/// trait of an observation which are simple transformation of `Data` struct
pub trait Observation {
    /// unique identifier of the observation
    fn to_int(&self) -> Option<usize> {
        None
    }
    /// observation feature the order is: tree observation (tree_1, tree_2 ...), excavator speed, excavator angular speed, stick extension, stick angle, finger state
    fn to_vec(&self) -> Option<Vec<f32>> {
        None
    }
    /// number of trees at proximity of the operator
    fn tree_observation(&self) -> TreeObservation;
    fn excavator_speed_observation(&self) -> String;
    fn excavator_speed_direction_observation(&self) -> Option<String> {
        None
    }
    fn excavator_angular_speed_observation(&self) -> String;
    fn stick_extension_observation(&self) -> Option<String>;
    fn stick_angle_observation(&self) -> String;
    fn finger_state_observation(&self) -> Option<String>;
}

impl Debug for dyn Observation {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let excavator_speed_direction =
            if let Some(direction) = self.excavator_speed_direction_observation() {
                direction
            } else {
                String::from("unavailable")
            };
        write!(f, "Observation {{ tree_observation: {tree_observation:?}, excavator_speed: {excavator_speed},excavator_speed_direction: {excavator_speed_direction}, excavator_angular_speed: {excavator_angular_speed} stick_extension:{stick_extension}, stick_angle: {stick_angle}, finger_state:{finger_state} }} ",
         tree_observation = self.tree_observation(),
         excavator_speed = self.excavator_speed_observation(),
         excavator_speed_direction = excavator_speed_direction,
         excavator_angular_speed = self.excavator_angular_speed_observation(),
         stick_extension = if let Some(el) = self.stick_extension_observation(){el}else{String::from("unavailable")},
         stick_angle = self.stick_angle_observation(),
         finger_state = if let Some(el) = self.finger_state_observation(){el} else{String::from("unavailable")}

        )
    }
}

#[derive(Debug)]
/// type of tree observation
pub enum TreeObservation {
    NumberOfTreesClose(String),
    DistanceOfTrees(Vec<String>),
    ClosestTree(String),
    None,
}

#[derive(Clone, Deserialize, Serialize)]
pub enum ObservationType {
    DiscreteObservation,
    ContinuousObservation,
    ContinuousObservationClosestTree,
    ContinuousObservationNoTree,
    ContinuousObservationSimpleMachineInformation,
}

pub fn new_observation(
    observation_type: &ObservationType,
    data: &[Data; 2],
) -> Box<dyn Observation> {
    match observation_type {
        ObservationType::DiscreteObservation => Box::new(DiscreteObservation::new(data)),
        ObservationType::ContinuousObservation => Box::new(ContinuousObservation::new(data)),
        ObservationType::ContinuousObservationClosestTree => {
            Box::new(ContinuousObservationClosestTree::new(data))
        }
        ObservationType::ContinuousObservationNoTree => {
            Box::new(ContinuousObservationNoTree::new(data))
        }
        ObservationType::ContinuousObservationSimpleMachineInformation => {
            Box::new(ContinuousObservationSimpleMachineInformation::new(data))
        }
    }
}

pub fn new_observation_vector(
    data: Vec<Data>,
    observation_type: &ObservationType,
    n_step: usize,
) -> Result<Vec<Box<dyn Observation>>, &'static str> {
    if data.len() < 2 {
        return Err("the len of data must be at least 2");
    }
    let mut resp: Vec<Box<dyn Observation>> = Vec::with_capacity((data.len() / 2) + 1);
    let mut prev_data = data[0].clone();
    let iter = data.iter().skip(1).step_by(n_step);
    for el in iter {
        let current_data = [prev_data, el.clone()];
        let obs = new_observation(&observation_type, &current_data);
        prev_data = el.clone();
        resp.push(obs);
    }
    Ok(resp)
}

pub fn new_observation_feature_vector(
    data: Vec<Data>,
    observation_type: ObservationType,
    n_step: usize,
) -> Result<Vec<Vec<f32>>, &'static str> {
    if data.len() < 2 {
        return Err("the len of data must be at least 2");
    } else if is_discrete(&observation_type) {
        return Err("must be a continuous type");
    }
    let mut resp: Vec<Vec<f32>> = Vec::with_capacity((data.len() / 2) + 1);
    let mut prev_data = data[0].clone();
    let iter = data.iter().skip(1).step_by(n_step);
    for el in iter {
        let current_data = [prev_data, el.clone()];
        let obs = new_observation(&observation_type, &current_data);
        prev_data = el.clone();
        resp.push(obs.to_vec().unwrap());
    }
    Ok(resp)
}

/// return the observation and the number of possible state
pub fn new_observation_int_vector(
    data: Vec<Data>,
    observation_type: ObservationType,
    n_step: usize,
) -> Result<(Vec<usize>, usize), &'static str> {
    if data.len() < 2 {
        return Err("the len of data must be at least 2");
    } else if !is_discrete(&observation_type) {
        return Err("must be a discrete type");
    }
    let mut resp = Vec::with_capacity((data.len() / 2) + 1);
    let mut prev_data = data[0].clone();
    let iter = data.iter().skip(1).step_by(n_step);
    for el in iter {
        let current_data = [prev_data, el.clone()];
        let obs = new_observation(&observation_type, &current_data);
        prev_data = el.clone();
        resp.push(obs.to_int().unwrap());
    }
    Ok((
        resp,
        number_possible_observation(&observation_type).unwrap(),
    ))
}

fn is_discrete(observation_type: &ObservationType) -> bool {
    match observation_type {
        ObservationType::DiscreteObservation => true,
        _ => false,
    }
}

pub fn number_possible_observation(observation_type: &ObservationType) -> Option<usize> {
    match observation_type {
        ObservationType::DiscreteObservation => {
            Some(DiscreteObservation::number_possible_observation())
        }
        _ => None,
    }
}
