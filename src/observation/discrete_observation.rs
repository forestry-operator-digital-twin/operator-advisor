use super::{Observation, TreeObservation};

use super::util::{
    data_trees_to_vector, distance_between_points, distance_between_points_2d, norm,
};
use crate::InternalData as Data;
use enum_ordinalize::*;
use lazy_static::*;
use std::collections::hash_map::HashMap;
use strum::{EnumCount, IntoEnumIterator};
use strum_macros::{EnumCount as EnumCountMacro, EnumIter};

#[derive(Debug, Clone)]
/// create an observation with discrete values that implement `Observation` trait
pub struct DiscreteObservation {
    tree_observation: TreesClose,
    finger_observation: FingerObservation,
    excavator_speed: ExcavatorSpeed,
    excavator_angular_speed: ExcavatorAngularSpeed,
    stick_position: StickExtension,
    stick_angle: StickAngularPosition,
}

impl DiscreteObservation {
    pub fn new(data: &[Data; 2]) -> Self {
        Self {
            tree_observation: DiscreteObservation::observation_number_trees_close(&data[1]),
            finger_observation: DiscreteObservation::observation_finger(&data[1]),
            excavator_speed: DiscreteObservation::observation_exacavator_speed(data),
            excavator_angular_speed: DiscreteObservation::observation_exacavator_angular_speed(
                data,
            ),
            stick_position: DiscreteObservation::observation_stick_position(data),
            stick_angle: DiscreteObservation::observation_stick_angle(data),
        }
    }

    fn observation_number_trees_close(data: &Data) -> TreesClose {
        let mut counter = 1i8;
        for tree_position in data_trees_to_vector(data) {
            let distance = distance_between_points(tree_position, data.position_excavator);
            if distance <= CLOSE_TREE_DISTANCE {
                counter += 1;
            }
        }
        if counter < TreesClose::COUNT as i8 {
            TreesClose::from_ordinal(counter).unwrap()
        } else {
            TreesClose::Multiple
        }
    }

    fn observation_finger(data: &Data) -> FingerObservation {
        let distance = distance_between_points(data.position_finger_1, data.position_finger_2);

        if distance <= DISTANCE_CLOSE_FINGER {
            FingerObservation::Close
        } else {
            FingerObservation::Open
        }
    }

    fn observation_exacavator_speed(data: &[Data; 2]) -> ExcavatorSpeed {
        let speed = (norm(data[0].speed_excavator) + norm(data[1].speed_excavator)) / 2f32;

        if speed >= FAST_EXCAVATOR_SPEED {
            return ExcavatorSpeed::Fast;
        } else if speed >= SLOW_EXCAVATOR_SPEED {
            return ExcavatorSpeed::Slow;
        } else {
            ExcavatorSpeed::Still
        }
    }

    fn observation_exacavator_angular_speed(data: &[Data; 2]) -> ExcavatorAngularSpeed {
        let speed = ((data[0].angular_speed_chassis + data[1].angular_speed_chassis) / 2f32).abs();

        if speed >= FAST_EXCAVATOR_ANGULAR_SPEED {
            ExcavatorAngularSpeed::Fast
        } else if speed >= SLOW_EXCAVATOR_ANGULAR_SPEED {
            ExcavatorAngularSpeed::Slow
        } else {
            ExcavatorAngularSpeed::Still
        }
    }

    fn observation_stick_position(data: &[Data; 2]) -> StickExtension {
        let distance =
            (distance_between_points_2d(data[0].position_excavator, data[0].position_finger_1)
                + distance_between_points_2d(
                    data[0].position_excavator,
                    data[1].position_finger_1,
                ))
                / 2f32;

        if distance >= FAR_STICK_DISTANCE {
            StickExtension::Far
        } else if distance >= MIDDLE_STICK_DISTANCE {
            StickExtension::Middle
        } else {
            StickExtension::Close
        }
    }

    fn observation_stick_angle(data: &[Data; 2]) -> StickAngularPosition {
        let mut angle = (data[0].angular_pos_chassis + data[1].angular_pos_chassis) / 2f32;
        // convert rad to deg
        angle *= 180f32 / std::f32::consts::PI;
        angle = if angle < 0f32 { 360f32 - angle } else { angle };
        if angle >= 270f32 {
            StickAngularPosition::AcutePositiveNegative
        } else if angle >= 180f32 {
            StickAngularPosition::ObtuseNegative
        } else if angle >= 90f32 {
            StickAngularPosition::ObtusePositive
        } else {
            StickAngularPosition::AcutePositive
        }
    }

    pub fn number_possible_observation() -> usize {
        lazy_static! {
            static ref N: usize = TreesClose::COUNT
                * FingerObservation::COUNT
                * ExcavatorSpeed::COUNT
                * ExcavatorAngularSpeed::COUNT
                * StickExtension::COUNT
                * StickAngularPosition::COUNT;
        }
        *N
    }

    fn all_posibilities() -> HashMap<CombinatorialObservation, usize> {
        // create the posibility map at compilation time
        lazy_static! {
            static ref ALL_POSIBILITIES: HashMap<CombinatorialObservation, usize> = {
                let mut m = HashMap::new();
                let mut i: usize = 0;

                for tree_close in TreesClose::iter() {
                    for finger_observation in FingerObservation::iter() {
                        for excavator_speed in ExcavatorSpeed::iter() {
                            for excavator_angular_speed in ExcavatorAngularSpeed::iter() {
                                for boom_extension in StickExtension::iter() {
                                    for boom_angular_position in StickAngularPosition::iter() {
                                        m.insert(
                                            (
                                                tree_close.clone(),
                                                finger_observation.clone(),
                                                excavator_speed.clone(),
                                                excavator_angular_speed.clone(),
                                                boom_extension.clone(),
                                                boom_angular_position.clone(),
                                            ),
                                            i,
                                        );
                                        i += 1;
                                    }
                                }
                            }
                        }
                    }
                }
                m
            };
        }
        ALL_POSIBILITIES.clone()
    }
}

impl Observation for DiscreteObservation {
    fn to_int(&self) -> Option<usize> {
        let observation: CombinatorialObservation = (
            self.tree_observation.clone(),
            self.finger_observation.clone(),
            self.excavator_speed.clone(),
            self.excavator_angular_speed.clone(),
            self.stick_position.clone(),
            self.stick_angle.clone(),
        );
        Some(DiscreteObservation::all_posibilities()[&observation])
    }

    fn tree_observation(&self) -> TreeObservation {
        TreeObservation::NumberOfTreesClose(format!("{:?}", self.tree_observation))
    }

    fn excavator_speed_observation(&self) -> String {
        format!("{:?}", self.excavator_speed)
    }

    fn excavator_angular_speed_observation(&self) -> String {
        format!("{:?}", self.excavator_angular_speed)
    }

    fn stick_extension_observation(&self) -> Option<String> {
        Some(format!("{:?}", self.stick_position))
    }

    fn stick_angle_observation(&self) -> String {
        format!("{:?}", self.stick_angle)
    }

    fn finger_state_observation(&self) -> Option<String> {
        Some(format!("{:?}", self.finger_observation))
    }
}

type CombinatorialObservation = (
    TreesClose,
    FingerObservation,
    ExcavatorSpeed,
    ExcavatorAngularSpeed,
    StickExtension,
    StickAngularPosition,
);
#[derive(EnumCountMacro, Ordinalize, Debug, EnumIter, Eq, PartialEq, Hash, Clone)]
enum TreesClose {
    Zero,
    One,
    Two,
    Multiple,
}

#[derive(EnumCountMacro, Debug, EnumIter, Eq, PartialEq, Hash, Clone)]
enum FingerObservation {
    Open,
    Close,
}

#[derive(EnumCountMacro, Debug, EnumIter, Eq, PartialEq, Hash, Clone)]
enum ExcavatorSpeed {
    Fast,
    Slow,
    Still,
}

type ExcavatorAngularSpeed = ExcavatorSpeed;

#[derive(EnumCountMacro, Debug, EnumIter, Eq, PartialEq, Hash, Clone)]
enum StickExtension {
    Close,
    Middle,
    Far,
}

#[derive(EnumCountMacro, Debug, EnumIter, Eq, PartialEq, Hash, Clone)]
enum StickAngularPosition {
    AcutePositive,
    AcutePositiveNegative,
    ObtusePositive,
    ObtuseNegative,
}

const CLOSE_TREE_DISTANCE: f32 = 9.3;
const DISTANCE_CLOSE_FINGER: f32 = 0.4;

const FAST_EXCAVATOR_SPEED: f32 = 0.51;
const SLOW_EXCAVATOR_SPEED: f32 = 0.05;

const FAST_EXCAVATOR_ANGULAR_SPEED: f32 = 0.15;
const SLOW_EXCAVATOR_ANGULAR_SPEED: f32 = 0.05;

const FAR_STICK_DISTANCE: f32 = 8.5;
const MIDDLE_STICK_DISTANCE: f32 = 6.0;
