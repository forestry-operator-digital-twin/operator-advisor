use crate::observation::{
    new_observation_feature_vector, new_observation_vector, Observation, ObservationType,
};
use crate::Data;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs;
use std::path::PathBuf;
pub mod label;
use crate::observation::util as observation_helper;
use std::collections::hash_map::HashMap;
mod util;
use crate::model::util::*;

pub fn get_observations(
    paths: Vec<PathBuf>,
    n_step: usize,
    observation_type: &ObservationType,
) -> Result<Vec<Box<dyn Observation>>, String> {
    let mut v_obs: Vec<Box<dyn Observation>> = Vec::new();
    for path in paths {
        let data = Data::from_file(path.clone())?;
        if let Ok(mut current_obs) = new_observation_vector(data.data, observation_type, n_step) {
            v_obs.append(&mut current_obs);
        } else {
            return Err(String::from(
                "the observation cannot be converted into a int",
            ));
        }
    }
    Ok(v_obs)
}

pub fn get_all_observation_from_data_path(
    path: PathBuf,
    n_step_dataset: usize,
    observation_type: &ObservationType,
) -> Result<Vec<Box<dyn Observation>>, String> {
    let dataset_paths = get_all_data_path(path)?;
    get_observations(dataset_paths.clone(), n_step_dataset, observation_type)
}

pub fn get_training_observation_state(
    path: PathBuf,
    n_step: usize,
    observation_type: ObservationType,
) -> Result<TrainingDataObservationState, String> {
    let dataset_paths = get_all_data_path(path.clone())?;
    let label_paths = get_all_label_data_path(path.clone())?;

    let mut training_data = TrainingDataObservationState {
        data: HashMap::new(),
    };
    for (data_path, label_path) in
        associate_training_path(dataset_paths, label_paths.clone()).values()
    {
        let current_data = create_a_training_step(
            data_path.clone(),
            label_path.clone(),
            observation_type.clone(),
            n_step,
        )?;
        training_data.data.insert(
            String::from(data_path.to_str().unwrap()),
            current_data.clone(),
        );
    }

    Ok(training_data)
}

pub fn create_observation_file(
    path: PathBuf,
    n_step_dataset: usize,
    res_path: PathBuf,
    observation_type: &ObservationType,
) -> Result<(), String> {
    let dataset_paths = get_all_data_path(path)?;
    let ys = get_observations(dataset_paths.clone(), n_step_dataset, observation_type)?;
    let data = ObservationData {
        data: ys
            .iter()
            .map(move |x| {
                if let Some(data) = x.to_int() {
                    vec![data as f32]
                } else {
                    x.to_vec().unwrap()
                }
            })
            .collect(),
        data_type: observation_type.clone(),
    };
    match serde_json::to_string(&data) {
        Err(e) => Err(format!("{}", e)),
        Ok(j) => {
            fs::write(res_path, j).expect("Unable to write file");
            Ok(())
        }
    }
}

pub fn create_a_training_step(
    data_path: PathBuf,
    label_path: PathBuf,
    observation_type: ObservationType,
    n_step: usize,
) -> Result<Vec<InnerTrainingData>, String> {
    let data = Data::from_file(data_path.clone())?;
    let obs = new_observation_feature_vector(data.data, observation_type, n_step)?;

    let label = get_label(label_path.clone())?;
    let label: Vec<usize> = expend_states(&label, n_step);
    let mut training_data: Vec<InnerTrainingData> = Vec::new();

    let iter = obs
        .iter()
        .take(obs.len() - 1)
        .zip(label.iter().take(label.len() - 1))
        .zip(0..obs.len());
    // observation, state, index
    for ((o, s), i) in iter {
        let current_data = InnerTrainingData {
            observation: o.clone(),
            state: *s,
            next_observation: obs.get(i + 1).unwrap_or(o).clone(),
            next_state: *label.get(i + 1).unwrap_or(s),
        };
        training_data.push(current_data);
    }
    Ok(training_data.clone())
}

pub fn create_training_observation_state_file(
    path: PathBuf,
    res_path: PathBuf,
    n_step: usize,
    observation_type: ObservationType,
) -> Result<(), String> {
    let data = get_training_observation_state(path, n_step, observation_type)?;
    match serde_json::to_string(&data) {
        Err(e) => Err(format!("{}", e)),
        Ok(j) => {
            fs::write(res_path, j).expect("Unable to write file");
            Ok(())
        }
    }
}

pub fn get_training_data_observation(
    path: PathBuf,
    n_step: usize,
    observation_type: &ObservationType,
) -> Result<TrainingDataObservation, String> {
    let mut datas = TrainingDataObservation {
        data: HashMap::new(),
    };
    let dataset_paths = get_all_data_path(path.clone())?;
    for data_path in dataset_paths {
        let obs = get_observation(data_path.clone(), n_step, observation_type)?;
        datas.data.insert(
            String::from(data_path.to_str().unwrap()),
            observation_helper::convert_observations_to_feature_vector(&obs),
        );
    }
    Ok(datas)
}

pub fn create_training_data_continuous_observation_file(
    path: PathBuf,
    n_step_dataset: usize,
    res_path: PathBuf,
    observation_type: &ObservationType,
) -> Result<(), String> {
    let data = get_training_data_observation(path, n_step_dataset, observation_type)?;
    match serde_json::to_string(&data) {
        Err(e) => Err(format!("{}", e)),
        Ok(j) => {
            fs::write(res_path, j).expect("Unable to write file");
            Ok(())
        }
    }
}

#[derive(Deserialize, Serialize, Clone)]
/// All observation of every test from https://gitlab.com/forestry-operator-digital-twin/data
pub struct ObservationData {
    pub data: Vec<Vec<f32>>,
    pub data_type: ObservationType,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
/// Training data associating the state and the observation at every step
pub struct TrainingDataObservationState {
    pub data: HashMap<String, Vec<InnerTrainingData>>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct InnerTrainingData {
    pub observation: Vec<f32>,
    pub state: usize,
    pub next_observation: Vec<f32>,
    pub next_state: usize,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
/// training data associating the step with the observation
pub struct TrainingDataObservation {
    pub data: HashMap<String, Vec<Vec<f32>>>,
}
