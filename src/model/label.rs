use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
/// Represent the state of an observation from the data from the simulator https://gitlab.com/forestry-operator-digital-twin/data
pub struct Label {
    pub data: Vec<Inner>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Inner {
    pub from: usize,
    pub to: usize,
    pub state: usize,
}
